# This is a functioning JS Snake game #

## How to play: ##
* Z to go faster
* X to go slower
* SPACE to pause/unpause
* **LEFT**/**RIGHT**/**UP**/**DOWN** arrow keys to go, well,
left right up & down.
The important thing when playing this game is to remember that
when you press, say, **UP**, the first segment starts moving up;
every other segment goes where the segment before it was.
So, if the snake is going **UP** and the **+** is the
point that the snake is turning on:

```
#!

      |
      |
      |
- - - +

```
Turns into:

```
#!

      |
      |
      |
      |
  - - +
```
Turns into:
```
#!


      |
      |
      |
      |
      |
    - +
```
Turns into:
```
#!

      |
      |
      |
      |
      |
      |
      +
```

# Contribution #
### To contribute, just push 
```
#!

@TODO
```
 comments to wherever needed!