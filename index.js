var $ = jQuery || window.$;

$(document).ready(function() {


    var canvasElem = document.getElementById("canvas");
    var canvas = canvasElem.getContext('2d');

    $("#playSlow").click(function() {
        playSlowSpeed();
    });
    $("#playMedium").click(function() {
        playMediumSpeed();
    });
    $("#playFast").click(function() {
        playFastSpeed();
    });
    $("#playSuperFast").click(function() {
        playSuperFastSpeed();
    });
    $("#playSuperSuperFast").click(function() {
        playSuperSuperFastSpeed();
    });

    var width = 1900;
    var height = 900;
    var boxSize = 25;
    var gameOver = false;
    function playSlowSpeed() {
        playGame(width, height, boxSize, gameOver, canvas, 200);
    }
    function playMediumSpeed() {
        playGame(width, height, boxSize, gameOver, canvas, 150);
    }
    function playFastSpeed() {
        playGame(width, height, boxSize, gameOver, canvas, 100);
    }
    function playSuperFastSpeed() {
        playGame(width, height, boxSize, gameOver, canvas, 50);
    }
    function playSuperSuperFastSpeed() {
        playGame(width, height, boxSize, gameOver, canvas, 25);
    }
});