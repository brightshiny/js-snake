function playGame(width, height, boxSize, gameOver, canvas, millisUntilNextFrame) {
    $("#canvas").removeClass("hidden");
    document.getElementById("canvas").focus();
    $("#buttoncontainer").find('button').blur();
    $("#buttoncontainer").css('display', 'none');
    var snakeAr;
    var numOfFoods = 0;
    snakeAr = [
        //{x: 12, y: 0},
        //{x: 11, y: 0},
        //{x: 10, y: 0},
        //{x:  9, y: 0},
        //{x:  8, y: 0},
        //{x:  7, y: 0},
        //{x:  6, y: 0},
        //{x:  5, y: 0},
        //{x:  4, y: 0},
        //{x:  3, y: 0},
        {x:  2, y: 0},
        {x:  1, y: 0},
        {x:  0, y: 0}
    ];
    /*
     left = 37
     up = 38
     right = 39
     down = 40

     */
    document.onkeydown = checkKey;
    
    /**
     * Returns a random integer between min (inclusive) and max (inclusive)
     * Using Math.round() will give you a non-uniform distribution!
     */
    function getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    function overlaps(ar) {
        for(var i = 1; i < ar.length; i++) {
            if(ar[0].x == ar[i].x && ar[0].y == ar[i].y) return true;
        }
        return false;
    }

    var KEY_CODE_LEFT  = 37;
    var KEY_CODE_RIGHT = 39;
    var KEY_CODE_UP    = 38;
    var KEY_CODE_DOWN  = 40;
    var DIR_LEFT       = function(pos) { return {x: pos.x-1, y: pos.y}; };
    var DIR_RIGHT      = function(pos) { return {x: pos.x+1, y: pos.y}; };
    var DIR_UP         = function(pos) { return {x: pos.x, y: pos.y-1}; };
    var DIR_DOWN       = function(pos) { return {x: pos.x, y: pos.y+1}; };
    var direction = DIR_RIGHT;
    var paused = false;
    var food = {
        x: getRandomInt(0, width/boxSize-1),
        y: getRandomInt(0, height/boxSize-1)
    };
    function contains(ar, pos) {
        return (ar[0].x == pos.x && ar[0].y == pos.y);
    }

    function checkKey(e) {

        e = e || window.event;

        if (e.keyCode == '38' && direction != DIR_DOWN) {
            direction = DIR_UP;
        }
        else if (e.keyCode == '40' && direction != DIR_UP) {
            direction = DIR_DOWN;
        }
        else if (e.keyCode == '37' && direction != DIR_RIGHT) {
            direction = DIR_LEFT;
        }
        else if (e.keyCode == '39' && direction != DIR_LEFT) {
            direction = DIR_RIGHT;
        }
        else if(e.keyCode == '32') {
            if(!gameOver) {
                paused = !paused;
                if (paused == false) {
                    draw();
                }
            } // END IF
        } // END IF
        else if(e.keyCode == '90') {
            millisUntilNextFrame-=20;
        } else if(e.keyCode == '88') {
            millisUntilNextFrame+=20;
        }

    }

    function background() {
        canvas.fillStyle = 'black';
        canvas.fillRect(0, 0, width, height);
    }
    function moveSegment(_pos) {
        var pos = {x: _pos.x, y: _pos.y};
        if(direction == DIR_LEFT) {
            pos.x--;
        } else if(direction == DIR_RIGHT) {
            pos.x++;
        } else if(direction == DIR_UP) {
            pos.y--;
        } else if(direction == DIR_DOWN) {
            pos.y++;
        }
        return pos;
    }
    function shadeColor(color, percent) {

        var R = parseInt(color.substring(1,3),16);
        var G = parseInt(color.substring(3,5),16);
        var B = parseInt(color.substring(5,7),16);

        R = parseInt(R * (100 + percent) / 100);
        G = parseInt(G * (100 + percent) / 100);
        B = parseInt(B * (100 + percent) / 100);

        R = (R<255)?R:255;
        G = (G<255)?G:255;
        B = (B<255)?B:255;

        var RR = ((R.toString(16).length==1)?"0"+R.toString(16):R.toString(16));
        var GG = ((G.toString(16).length==1)?"0"+G.toString(16):G.toString(16));
        var BB = ((B.toString(16).length==1)?"0"+B.toString(16):B.toString(16));

        return "#"+RR+GG+BB;
    }
    function draw() {
        background();
        canvas.fillStyle = '#FFFFFF';
        canvas.font = "30px Monospace";
        canvas.textAlign = "left";
        canvas.fillText("Length: " + snakeAr.length, 10, 25);
        canvas.fillText("Score : " + calcScore()   , 10, 60);
        canvas.fillText("Foods : " + numOfFoods    , 10, 95);
        canvas.fillText("BPS   : " + Math.round(1000 / millisUntilNextFrame * 100) / 100, 10, 130);
        for(var i = snakeAr.length-1; i > -1; i--) {
            var snakeSegment = snakeAr[i];
            if (i == 0) {
                canvas.fillStyle = 'rgb(255, 255, 255)';
            } else {
                var fillPercentage = i * (100/snakeAr.length);
                canvas.fillStyle = shadeColor("#FF0000", -fillPercentage);
            }
            canvas.fillRect(snakeSegment.x*boxSize, snakeSegment.y*boxSize, boxSize, boxSize);

            if(snakeAr[i].x < 0 || snakeAr[i].x > width/boxSize-1 || snakeAr[i].y < 0 || snakeAr[i].y > height/boxSize-1 || overlaps(snakeAr)) {
                gameOver = true;
            }
        }
        canvas.fillStyle = shadeColor('#FFC0CB',0);
        canvas.fillRect(food.x*boxSize, food.y*boxSize, boxSize, boxSize);

        moveSnake();
        if(contains(snakeAr, food)) {
            numOfFoods++;
            snakeAr[snakeAr.length] = {
                x: snakeAr[snakeAr.length - 1].x,
                y: snakeAr[snakeAr.length - 1].y
            };
            for(var i = 1; i <= 5; i++) {
                setTimeout(function() {
                    snakeAr[snakeAr.length] = {
                        x: snakeAr[snakeAr.length - 1].x,
                        y: snakeAr[snakeAr.length - 1].y
                    };
                }, millisUntilNextFrame*(i+2));
            }
            food = {
                x: getRandomInt(0, width / boxSize - 1),
                y: getRandomInt(0, height / boxSize - 1)
            };
        }
        if(!gameOver) {
            if(!paused) {
                setTimeout(draw, millisUntilNextFrame);
            }
        } else {
            canvas.strokeStyle = 'rgb(255, 255, 255)';
            canvas.fillStyle = 'red';
            canvas.font = "60px Comic Sans MS";
            canvas.textAlign = "center";
            var text = "Game Over!\nScore: " + calcScore();
            canvas.fillText(text, width/2, height/2);
            canvas.lineWidth = 3;
            canvas.strokeText(text, width/2, height/2);
            setTimeout(function() {
                $("#canvas").addClass("hidden");
                $("#buttoncontainer").css('display', 'block');
            }, 3000);
        } // END IF
    } // END FUNC DRAW()
    function calcScore() {
        return Math.round(((snakeAr.length)) * 145 / (millisUntilNextFrame / 6));
    }
    /*************************************************************************
     * SPEED:   SLOW    MEDIUM      FAST    SUPER FAST      SUPER SUPER FAST
     * POINTS:  39      52          78      157             313
     * FOODS:   1       1           1       1               1
    *************************************************************************/

    function moveSnake() {
        var newSnakeAr = [];
        newSnakeAr[0] = moveSegment(snakeAr[0]);
        for(var i = 1; i < snakeAr.length; i++) {
            newSnakeAr[i] = snakeAr[i-1];
        }
        snakeAr = newSnakeAr.slice(0);
    }

    draw();
}